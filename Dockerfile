FROM golang:alpine AS builder

# Set necessary environmet variables needed for our image
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

# Move to working directory /build
WORKDIR /build
# Copy the code into the container
COPY src src
# Build the application
WORKDIR /build/src
# Install dependencies
RUN go get -d -v
# Build go app
RUN go build -o ../main main.go

# Move to /dist directory as the place for resulting binary folder
WORKDIR /dist

# Copy binary from build to main folder
RUN cp /build/main .

# Build a small image
FROM alpine
RUN mkdir -p env
COPY src/env  /env
COPY --from=builder /dist/main /
EXPOSE 3000
# Command to run
ENTRYPOINT ["/main"]