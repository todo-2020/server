package model

import (
	"time"
)

// Represent Todo item
type Task struct {
	ID       string    `json:"id"`
	Index  	 int	   `json:"index"`
	Done     int       `json:"done"`
	Todo     string    `json:"todo"`
	Created  time.Time `json:"created"`
	Modified time.Time `json:"modified"`
}

// use for create service
type CreateTaskInput struct {
	Todo string `json:"todo"`
}

// use for update service
type UpdateTaskInput struct {
	ID   string `json:"id"`
	Done int    `json:"done"`
	Todo string `json:"todo"`
}

// use for update service
type DeleteTaskInput struct {
	ID string `json:"id"`
}
