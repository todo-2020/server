package controller

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/todo-2020/server/src/database"
	"gitlab.com/todo-2020/server/src/model"
)

// POST /todos
// Create new todo item
func (ctrl *Controller) CraeteTask(c *gin.Context) {
	db := c.MustGet("taskDB").(*database.TaskDB)

	// Validate input
	var inputs []model.CreateTaskInput
	if err := c.ShouldBindJSON(&inputs); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Create task
	var response []model.Task
	for _, input := range inputs {
		task := model.Task{ID: db.GenerateId(), Todo: input.Todo, Created: time.Now(), Modified: time.Now()}
		err := db.Add(task)
		if err != nil {
			panic(err)
		}
		response = append(response, task)
	}
	c.JSON(http.StatusCreated, response)
}

// PUT /todos
// Update todo item
func (ctrl *Controller) UpdateTask(c *gin.Context) {
	db := c.MustGet("taskDB").(*database.TaskDB)

	// Validate input
	var input model.UpdateTaskInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Update task
	task := model.Task{ID: input.ID, Todo: input.Todo, Done: input.Done}
	err := db.Update(task)
	if err != nil {
		panic(err)
	}
	c.JSON(http.StatusCreated, task)
}

// GET /todos
// Get all todo items
func (ctrl *Controller) GetTasks(c *gin.Context) {
	db := c.MustGet("taskDB").(*database.TaskDB)
	tasks := db.GetAll()
	c.JSON(http.StatusOK, tasks)
}

// DELETE /todos
// DELETE
func (ctrl *Controller) DeleteTask(c *gin.Context) {
	db := c.MustGet("taskDB").(*database.TaskDB)
	// Validate input
	var input model.DeleteTaskInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := db.Delete(model.Task{ID: input.ID})
	if err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, true)
}
func (ctrl *Controller) DeleteAll(c *gin.Context) {
	db := c.MustGet("taskDB").(*database.TaskDB)
	tasks := db.GetAll()
	for _, item := range tasks {
		db.Delete(item)
	}
	c.JSON(http.StatusOK, true)
}
