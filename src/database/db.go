package database

import (
	"crypto/rand"
	"fmt"
	"log"

	"gitlab.com/todo-2020/server/src/model"
)

func (taskDB *TaskDB) GenerateId() string {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	uuid := fmt.Sprintf("%x-%x-%x-%x-%x",
		b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
	return uuid
}

// get all tasks
func (taskDB *TaskDB) GetAll() []model.Task {
	err := taskDB.Open()
	if err != nil {
		panic(err)
	}
	// rows, _ := taskDB.db.Query(GET_ALL_QUERY)
	var response []model.Task
	txn := taskDB.DB.Txn(false)
	defer txn.Abort()
	it, err := txn.Get("tasks", "id")

	for obj := it.Next(); obj != nil; obj = it.Next() {
		p := obj.(model.Task)
		response = append(response, p)
	}
	return response
}
func (taskDB *TaskDB) GetTodoByID(task model.Task) (*model.Task, error) {
	err := taskDB.Open()
	if err != nil {
		return nil, err
	}
	txn := taskDB.DB.Txn(false)
	defer txn.Abort()
	raw, err := txn.First("tasks", "id", task.ID)
	return raw.(*model.Task), nil
}

// add task to the database
func (taskDB *TaskDB) Add(task model.Task) error {
	err := taskDB.Open()
	if err != nil {
		return err
	}
	txn := taskDB.DB.Txn(true)
	task.Index = TODO_COUNT
	if err := txn.Insert("tasks", task); err != nil {
		panic(err)
	}
	txn.Commit()
	TODO_COUNT++
	return nil
}

// updaet task
func (taskDB *TaskDB) Update(task model.Task) error {
	err := taskDB.Open()
	if err != nil {
		return err
	}
	txn := taskDB.DB.Txn(true)
	if err := txn.Insert("tasks", task); err != nil {
		panic(err)
	}
	txn.Commit()
	return nil
}

// delete task
func (taskDB *TaskDB) Delete(task model.Task) error {
	err := taskDB.Open()
	if err != nil {
		return err
	}
	txn := taskDB.DB.Txn(true)
	txn.Delete("tasks", task)
	txn.Commit()
	return nil
}
