package database

import (
	"fmt"
	"github.com/hashicorp/go-memdb"
	"log"
	"os"
)

var TODO_COUNT = 0
// DB operations
type DBOperations interface {
	Open() (*TaskDB, error)
	Close() error
}

// wrapper for memdb
type TaskDB struct {
	DB     *memdb.MemDB
	schema *memdb.DBSchema
	Logger *log.Logger
	dbName string
}

func (taskDB*TaskDB) GetDbSchema() *memdb.DBSchema{
	 s :=  &memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			"tasks": &memdb.TableSchema{
				Name: "tasks",
				Indexes: map[string]*memdb.IndexSchema {
					"id": &memdb.IndexSchema{
						Name:    "id",
						Unique:  true,
						Indexer: &memdb.StringFieldIndex{Field: "ID"},
					},
				},
			},
		},
	}
	return s
}
// create new taskdb instance
func NewTaskDB(logger *log.Logger, dbName string) *TaskDB {
	return &TaskDB{Logger: logger, dbName: dbName}
}

// when applicaton starts, check if tables exist
func (taskDB *TaskDB) CreateDBIfNotExist(force bool) {
	dbPath := fmt.Sprintf("./%s.db", taskDB.dbName)
	if force {
		_, err := os.Stat(dbPath)
		if !os.IsNotExist(err) {
			err := os.Remove(dbPath)
			if err != nil {
				panic(err)
			}
		}
	}
	err := taskDB.Open()
	if err != nil {
		panic(err)
	}
	//cmd, _ := taskDB.db.Prepare(INIT_DB)
	//_, err = cmd.Exec()

	taskDB.DB,err = memdb.NewMemDB(taskDB.GetDbSchema())
	if err != nil {
		panic(err)
	}
}

// Open a new connection
func (taskDB *TaskDB) Open() error {
	//db, err := sql.Open("sqlite3", fmt.Sprintf("./%s.db", taskDB.dbName))
	//if err != nil {
	//	taskDB.logger.Fatalf("error occuered when accessing db, %#v", err)
	//	return err
	//}
	//taskDB.db = db
	return nil
}
