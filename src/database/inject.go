package database

import (
	"github.com/gin-gonic/gin"
)

// inject task db gin context
func Inject(taskDB *TaskDB) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("taskDB", taskDB)
		c.Next()
	}
}
