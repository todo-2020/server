package test

import (
	"net/http"
	"net/http/httptest"
)

func CreateGetAllTaskRequest() (*http.Request){
	req, _ := http.NewRequest("GET", todoRoot, nil)
	req.Header.Add("Content-Type", "application/json")
	return req
}
func (suite *TodoTestSuite) TestGetAllTasks() {
	// arrange
	req := CreateGetAllTaskRequest()
	w := httptest.NewRecorder()
	// act
	suite.api.ServeHTTP(w, req)
	// assert
	// var body string
	// body := w.Body.String()
	// log.Printf("Response is :=> %#v", body)
	suite.Equal(w.Code, http.StatusOK)
}
