package test

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/todo-2020/server/src/database"
)

// api for test
func InitDb() *database.TaskDB {
	logger := log.New(os.Stdout, "todo-server-test", log.LstdFlags)
	taskDB := database.NewTaskDB(logger, "task_test")
	taskDB.CreateDBIfNotExist(true)
	return taskDB
}
func CreateApiForTest() *gin.Engine {
	app := gin.Default()
	return app
}
