package test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/todo-2020/server/src/controller"
	"gitlab.com/todo-2020/server/src/database"
	"gitlab.com/todo-2020/server/src/model"
)

type TodoTestSuite struct {
	suite.Suite
	taskDB *database.TaskDB
	api    *gin.Engine
	ctrl   *controller.Controller
	todoId string
}

var todoRoot = "/api/v1/todo"

// setup database and api
func (suite *TodoTestSuite) SetupSuite() {
	// we initialize database and api for further tests
	suite.Init()
}
func (suite *TodoTestSuite) Init() {
	db := InitDb()
	suite.taskDB = db
	suite.api = CreateApiForTest()
	suite.api.Use(database.Inject(db))
	suite.api.POST(todoRoot, suite.ctrl.CraeteTask)
	suite.api.PUT(todoRoot, suite.ctrl.UpdateTask)
	suite.api.GET(todoRoot, suite.ctrl.GetTasks)
	suite.api.DELETE(todoRoot, suite.ctrl.DeleteTask)
}
func (suite *TodoTestSuite) AddItem(todo string) *httptest.ResponseRecorder {
	// arrange
	var jsonStr = fmt.Sprintf(`[{"todo":"%s"}]`, todo)
	req, _ := http.NewRequest("POST", todoRoot, strings.NewReader(jsonStr))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(jsonStr)))
	// act
	w := httptest.NewRecorder()
	suite.api.ServeHTTP(w, req)
	return w
}
func (suite *TodoTestSuite) TestAddTodoItem() {
	suiteT := suite.T()
	suite.Run("Add task to the empthy list", func() {
		todo := "buy some milk"
		w := suite.AddItem(todo)
		// assert
		var response []model.Task
		err := json.Unmarshal([]byte(w.Body.String()), &response)
		if err != nil {
			panic(err)
		}
		suite.todoId = response[0].ID
		assert.Equal(suiteT, response[0].Todo, todo)
	})
	suite.Run("Add task to the existing list", func() {
		// arrange
		todo := "enjoy the assignment"
		var jsonStr = fmt.Sprintf(`[{"todo":"%s"}]`, todo)
		req, _ := http.NewRequest("POST", todoRoot, strings.NewReader(jsonStr))
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Content-Length", strconv.Itoa(len(jsonStr)))
		// act
		w := httptest.NewRecorder()
		suite.api.ServeHTTP(w, req)
		// assert
		expected := map[int]string{
			0: "buy some milk",
			1: "enjoy the assignment",
		}
		w = httptest.NewRecorder()
		reqAll := CreateGetAllTaskRequest()
		suite.api.ServeHTTP(w, reqAll)
		var response []model.Task
		err := json.Unmarshal([]byte(w.Body.String()), &response)
		if err != nil {
			panic(err)
		}
		for _, r := range response {
			assert.Equal(suiteT, r.Todo, expected[r.Index])
		}
	})
	suite.Equal(suiteT, suite.T())
}

// start test
func TestSuite(t *testing.T) {
	suite.Run(t, new(TodoTestSuite))
}
